Setting up the project:

$ npm i -g create-react-app

$ create-react-app client && cd client

$ npm i -S axios

$ del src\App.css src\App.test.js src\index.css src\logo.svg

$ mkdir backend && cd backend

$ npm init

$ npm i -S mongoose express body-parser morgan

$ npm init -y

$ npm i -S concurrently
 
$ npm start

